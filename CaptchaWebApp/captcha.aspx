﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="captcha.aspx.cs" Inherits="CaptchaWebApp.captcha" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>

<!DOCTYPE html>
<script runat="server">

    public void btnSubmitClick(Object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            lblResult.Text = "OK";
        }
        else
        {
            lblResult.Text = "Error";
        }
    }

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form runat="server">
    <div>
        <asp:Label Visible=true ID="lblResult" runat="server" />
        <recaptcha:RecaptchaControl
              ID="recaptcha"
              runat="server"
              Theme="red"
              PublicKey="6Lfu_vsSAAAAAGL1lB62ifMWZ5cjJ0lLodS6rsjL"
              PrivateKey="6Lfu_vsSAAAAAEnfg4IVrhdguJ-X4RZ8kiy_oyUv"
              />
        
        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmitClick" />
    </div>
    </form>
</body>
</html>
